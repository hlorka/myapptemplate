#!/bin/bash

# Configure script launcher for OpenWRT-SDK
# Запускатр скрипта ./configure под микрокомпьютер Onion Omega под Linux OpenWRT SDK под платформу MIPS
# Скрипт составлен на основе документации: https://openwrt.org/ru/doc/devel/crosscompile

# define the pathes (by default) to the buildroot
BUILDROOT_PATH="/home/vladimir/OpenWRT-sdk"


# BEGIN: MIPS-only
# define the toolchain and target names
CROSS_COMPILE_HOST="mipsel-openwrt-linux"
STAGING_DIR="$BUILDROOT_PATH/staging_dir"
TOOLCHAIN_DIR="$STAGING_DIR/toolchain-mipsel_24kc_gcc-7.3.0_musl"
TARGET_DIR="$STAGING_DIR/target-mipsel_24kc_musl"
# END: MIPS-only

# define the toolchain paths
TOOLCHAIN_BIN="$TOOLCHAIN_DIR/bin"
TOOLCHAIN_INCLUDE="$TOOLCHAIN_DIR/include"
TOOLCHAIN_LIB="$TOOLCHAIN_DIR/lib"
TOOLCHAIN_USR_INCLUDE="$TOOLCHAIN_DIR/usr/include"
TOOLCHAIN_USR_LIB="$TOOLCHAIN_DIR/usr/lib"
# TOOLCHAIN_OPENWRTMIPS_INCLUDE="$TOOLCHAIN_DIR/mipsel-openwrt-linux-musl/include/c++/7.3.0"

# define the target paths
TARGET_USR_INCLUDE="$TARGET_DIR/usr/include"
TARGET_USR_LIB="$TARGET_DIR/usr/lib"

export PATH="$TOOLCHAIN_BIN:$PATH"
export STAGING_DIR

# define the FLAGS
TC_INCLUDES="-I$TOOLCHAIN_USR_INCLUDE -I$TOOLCHAIN_INCLUDE -I$TARGET_USR_INCLUDE -I$TARGET_USR_LIB"
TC_LDFLAGS="-L$TARGET_USR_LIB -L$TOOLCHAIN_LIB -L$TOOLCHAIN_USR_LIB"

./configure --build=x86_64-unknown-linux-gnu --host=$CROSS_COMPILE_HOST LDFLAGS="$TC_LDFLAGS" CFLAGS="$TC_INCLUDES" CXXFLAGS="$TC_INCLUDES" $*
