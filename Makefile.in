# Рецепт компиллирования программы на С++ для утилиты GNU Make.
#
# Доступные команды:
#
# make [-e debug=1]
# make all [-e debug=1]
#	Скомпиллировать программу.
#       Опция 'debug' компиллирует программу для отладчика и без оптимизации.
#
# make rebuild [-e debug=1]
#       Пересобрать программу, сначала удаляются все выходные и промежуточные ф$
#       Опция 'debug' компиллирует программу для отладчика и без оптимизации.
#
# make clean [-e debug=1]
#       Очистить текущую конфигурацию выходных файлов (Release или Debug):
#       если 'debug=1' будет удалена папка Debub/ в ином случае (по-умолчанию) $
#
# make clean-all
#       Очистить все конфигурации сборок выходных файлов (Release, Debug)
#
# make distclean
#       Выполнить 'clean-all' и дополнительно удалить файлы от утилиты GNU Auto$
#       Остаются только файлы дистрибьюции исходных кодов программы.
#
# make install
# make reinstall
#       Инсталлировать программу локально в текущую систему.
#       Версия 'reinstall' сначала вызывает 'uninstall' ниже
#       Не работает в режиме кросс-компиллции
#
# make uninstall
#	Деинсталлировать программу из текущей системы
#	Не работает в режиме кросс-компиллции
#
# make remote-install
# make remote-uninstall
# make remote-reinstall
#	Аналогично семейству команд выше, применительно для установки в удаленную систему.
#       Для кросс-компиляции рекомендуется скрипт 'xmake.sh' или аналогичный вместо 'make'
#	ПРИМЕЧАНИЕ: Доступно только с кросс-компилятором. Необходимо задать переменные
#	'REMOTE_HOST', 'REMOTE_HOST_USER' также настроить ssh-доступ по ключу для утилит 'ssh' и 'rsync', чтобы копирование производить без запроса пароля
#----------------------------------------------------------------------------

# Имя конечного исполняемого файла программы
APPFILENAME := @PACKAGE_NAME@

# Перечень каталогов, не считая текущего, с исходными файлами (.cpp, .c, .h и т.п.).
# Перечисляется через пробел; точка текущей директории не указывается.
SRCDIRS := src src/2

# Список дополнительно включаемых директорий h-файлов.
#     Каждая директория должна быть отделена пробелом.
#     Каждая директория должна оканчиваться прямым слешем.
#     Директории, содержащие символ пробела в имени помещаются в двойные кавычки.
EXTRAINCDIRS = 

# Директория вывода объектных файлов финальной версии приложения
#     Используйте '.' для вывода в текущую директорию (не рекомендуется)
#     Данный параметр не может быть пустым
RELEASE_DIR := Release

# Директория вывода объектных файлов отладочной версии приложения
#     Используйте '.' для вывода в текущую директорию (не рекомендуется)
#     Данный параметр не может быть пустым
DEBUG_DIR := Debug

# Адрес хоста и имя пользователя для удаленной установки
#     Действительно только для кросс-компиляции
REMOTE_HOST := 192.168.31.123
REMOTE_HOST_USER := root

# Уровень оптимизации [0 (без оптимизации - для отладки), 1, 2, 3, s (наименьший размер файла)]. 
OPT_LVL = 2

# Опции, работающие, только для отладки (при наличии в командной строке опции -e debug=1)
# При наличии переменной 'debug' назначается другая директория вывода (аргумент утилиты make .... -e debug)
ifdef debug
	override OUTDIR := $(DEBUG_DIR)
else
	OUTDIR := $(RELEASE_DIR)
endif

override cross_compile := @is_cross_compile@

# Переменные команд на исполнение
SHELL = @SHELL@
CXX = @CXX@
RM := rm
MKDIR := mkdir


# Перечень каталогов с фалами проекта
VPATH = $(SRCDIRS)

# Создание списка всех исходных файлов C++ во всех поддиректориях проекта с сохранением относительных путей.
cpp_files := $(wildcard $(addsuffix /*.cpp, . $(addprefix ./, $(SRCDIRS))))

# Список подкаталогов объектныъ файлов.
obj_dirs := $(OUTDIR) $(addprefix $(OUTDIR)/, $(SRCDIRS))


# Подпрограмма компилляции всех файлов CPP в объектные файлым (*.o).
# Аргумент №1 - список директорий с объектнми файлами
define app-compile
CXXFLAGS := $(filter -I%, @CXXFLAGS@) \
  $(addprefix -I,$(SRCDIRS))
ifdef debug
  CXXFLAGS += -g -DDEBUG
  override OPT_LVL := 0
else
  CXXFLAGS += -DNDEBUG
endif

CXXFLAGS += -O$(OPT_LVL) \
  -std=gnu++11 \
  -finput-charset=UTF-8 \
  -pipe \
  -funsigned-bitfields \
  -Wall \
  -Wundef
#CXXFLAGS += -Wa,-adhlns=$(<:%.CXX=$(OUTDIR)/%.lst)
include $(foreach dir,$(1),$(wildcard $(dir)/*.d))
endef

$(OUTDIR)/%.@OBJEXT@ : %.cpp
	$(CXX) -c $< -o $@ $(CXXFLAGS) -MMD -MF $@.d


# Подпрограмма сборки конечного (target) исполняемого файла.
# Аргумент №1 - путь и имя конечного бинарного (исполняемого) файла приложения
define app-link
# Linker's flags (options)
LDFLAGS = @LDFLAGS@ \
	-Wl,--relax

ifdef debug
  LDFLAGS += -Wl,-Map=$(1).map,--cref \
	-Xlinker -assert -Xlinker definitions
else
  LDFLAGS += -s
endif

obj_files = $(addprefix $(OUTDIR)/, $(patsubst ./%.cpp,%.@OBJEXT@, $(cpp_files)))

$(1): $(eval $(call app-compile,$(obj_dirs))) $$(obj_files)
	@echo Linking...
	$(CXX) $$^ $(CXXFLAGS) $(LDFLAGS) -o $$@
endef



# Цель по-умолчанию
all: gccversion build
	@echo The application '$(APPFILENAME)' succesefull built!

build: $(obj_dirs) $(eval $(call app-link,$(OUTDIR)/$(APPFILENAME))) $(OUTDIR)/$(APPFILENAME)

rebuild: clean all

gccversion:
	@$(CXX) --version

.PHONY : all build rebuild gccversion

# Создание подкаталогов объектных файлов
$(obj_dirs) :
	$(MKDIR) $@

# Команда чистки директории вывода (Relese или Debug)
clean:
	@echo Delete current configuration out directory.
	$(RM) -rdf $(OUTDIR)/

clean-all:
	@echo Delete all configuration out directories.
	rm -rdf $(RELEASE_DIR)/ $(DEBUG_DIR)/

clean-configure: clean-all
	$(RM) -f config.log config.h xmake.sh Makefile

clean-ac: clean-configure
	$(RM) -rdf autom4te.cache
	$(RM) -f configure config.*
	@echo To generate new configuration scripts please use GNU Autotools (autoreconf -iv)

.PHONY : clean clean-all clean-ac clean-configure


# ** СЕКЦИЯ УСТАНОВКИ **
# Переменные для целей инсталляции/деинсталляции
prefix := @prefix@
exec_prefix := @exec_prefix@

# Перечень файлов, устанавливаемых в директорию '@bindir@'
bin_PROGRAMS := $(OUTDIR)/$(APPFILENAME)


define install_create_target
nextTarget = $(1)/$(notdir $(2))
install_targets += $$(nextTarget)
ifneq ($(cross_compile), yes)
.DELETE_ON_ERROR: $$(nextTarget)
$$(nextTarget): $(2)
	install -p -t $$(@D) $$^
endif
endef

# Настройка копируемых файлов инсталляции-деинсталляции
# Генерация целей инсталляции в директорию '@bindir@'
$(foreach curFile,$(bin_PROGRAMS),\
	$(eval $(call install_create_target,@bindir@,$(curFile))))


# Targets for cross-compile-only
ifeq ($(cross_compile), yes)

remote-install: $(bin_PROGRAMS)
	rsync -tlpov $(bin_PROGRAMS) $(REMOTE_HOST_USER)@$(REMOTE_HOST):@bindir@
	@echo Remote installation successful.

remote-uninstall:
	ssh $(REMOTE_HOST_USER)@$(REMOTE_HOST) $(RM) -f $(install_targets)
	@echo Remote uninstallation is done successful!

remote-reinstall: remote-uninstall remote-install

.PHONY : remote-install remote-uninstall remote-reinstall
else

# Targets for normal (native) compile only.
install: $(install_targets)
	@echo The software successfuly installed!

uninstall:
	$(RM) -f $(install_targets)
	@echo The software successfuly uninstalled!

reinstall: uninstall install

.PHONY : install uninstall reinstall
endif

# For Automake compatibility only
EMPTY_AUTOMAKE_TARGETS := dvi pdf ps info html tags ctags distdir
.PHONY: $(EMPTY_AUTOMAKE_TARGETS)
$(EMPTY_AUTOMAKE_TARGETS):
